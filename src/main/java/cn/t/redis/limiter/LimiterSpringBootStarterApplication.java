package cn.t.redis.limiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LimiterSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(LimiterSpringBootStarterApplication.class, args);
    }

}
