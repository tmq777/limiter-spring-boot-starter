package cn.t.redis.limiter.web;

import cn.t.redis.limiter.annotations.RateLimit;
import cn.t.redis.limiter.components.RateLimiter;
import cn.t.redis.limiter.components.RedisOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class DemoController {

    @Autowired
    private RateLimiter limiter;

    @Autowired
    private RedisOperator operator;

    @RequestMapping("index")
    @RateLimit(interfaceName = "index", maxPermits = 1, tokensPerSeconds = 1)
    public String index() {
        operator.set("test1", new Date(), 20);
        return "hello world";
    }
}
