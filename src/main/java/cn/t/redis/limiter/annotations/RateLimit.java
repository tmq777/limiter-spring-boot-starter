package cn.t.redis.limiter.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 限流功能的注解
 */
@Documented
@Retention(RUNTIME)
@Target(value = {ElementType.METHOD})
public @interface RateLimit {

    /**
     * 限流接口名称
     * @return 限流接口名称
     */
    String interfaceName();

    /**
     * 最大令牌数
     * @return 最大令牌数
     */
    long maxPermits();

    /**
     * 每秒生成的令牌数
     * @return
     */
    long tokensPerSeconds();
}
