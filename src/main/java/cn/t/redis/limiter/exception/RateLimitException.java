package cn.t.redis.limiter.exception;

/**
 * 限流异常
 */
public class RateLimitException extends RuntimeException {

    public RateLimitException() {}

    public RateLimitException(String message) {
        super(message);
    }
}
